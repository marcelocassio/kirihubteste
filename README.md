## Sistema para emular uma compra parcelada
        O sistema vai receber uma serie de dados de compra como: o valor do produto, 
        o valor da entrada e a quantidade de parcelas. Ele vai devolver o valor de cada parcela e a taxa de juros aplicada.



### Reference Documentation

* [Documentacao Swagger : (http://localhost:8080/swagger-ui.html#/)](http://localhost:8080/swagger-ui.html#/)
* Os testes foram básicos somente para testar o funcionamento de uma só função, de maneira simples.
* Não foi aplicado nenhum tipo de persistencia.


## Pasos para executar:
```
* Clonar o repositorio (git clone git@gitlab.com:marcelocassio/kirihubteste.git)
* Importar o projeto como projeto Maven
* Atualizar as library de maven
* Correr o programa como Spring boot App
* Realizar as requisições desde um cliente como Posman ou no navegador. 
```



### Deixo por aqui também dois links de repositório de outro teste que fiz, no qual tem conexão com banco de dados e alguma complexidade a mais:


***Backend:***
*https://gitlab.com/marcelocassio/testshift_backend*

***FrontEnd:***
*https://gitlab.com/marcelocassio/teste-gupy-front*


### Enpoint local:
http://localhost:8080/api/gerarPagamento



### Exemplo Json entrada - Com 3 parcelas:
```
{ "produto": 
    { "codigo": 123, "nome": "Nome do Produto", "valor": 700 },
    "condicaoPagamento": 
     { "valorEntrada": 100, "qtdeParcelas": 3 }
}
```
### Exemplo Json saida:
```
[
    {
        "numeroParcela": 1,
        "valor": 200.0,
        "taxaJurosAoMes": 0.0
    },
    {
        "numeroParcela": 2,
        "valor": 200.0,
        "taxaJurosAoMes": 0.0
    },
    {
        "numeroParcela": 3,
        "valor": 200.0,
        "taxaJurosAoMes": 0.0
    }
]
```

### Exemplo Json entrada - Com 12 parcelas:
```
{ "produto": 
    { "codigo": 123, "nome": "Nome do Produto", "valor": 14000.00 },
    "condicaoPagamento": 
     { "valorEntrada": 400, "qtdeParcelas": 12 }
}
```


### Exemplo Json saida:
```
[
    {
        "numeroParcela": 1,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 2,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 3,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 4,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 5,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 6,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 7,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 8,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 9,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 10,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 11,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    },
    {
        "numeroParcela": 12,
        "valor": 1187.73,
        "taxaJurosAoMes": 0.4
    }
]
```


# Controle de erros - Saída Json
## Valor de produto <= 0 :
### Input
```
{ "produto":
{ "codigo": 123, "nome": "Nome do Produto", "valor": 0 },
"condicaoPagamento":
{ "valorEntrada": 10, "qtdeParcelas": 12 }
}
```

### Output
```
{
    "error: ": "O valor do produto é invalido. Por favor verifique."
}
```

## Valor da entrada > valor do produto:

### Input
```
{ "produto":
{ "codigo": 123, "nome": "Nome do Produto", "valor": 100 },
"condicaoPagamento":
{ "valorEntrada": 110, "qtdeParcelas": 12 }
}
```

### Output

```
{
    "error: ": "O valor da entrada nao pode ser maior que o valor do produto."
}
```

## Valor entrada <= 0

### Input
```
{ "produto":
{ "codigo": 123, "nome": "Nome do Produto", "valor": 100 },
"condicaoPagamento":
{ "valorEntrada": -10, "qtdeParcelas": 10 }
}
```

### Output
```
{
    "error: ": "O valor da entrada é invalido. Por favor verifique."
}
```

## Quantidade parcelas = 0 ##

### Input
```
{ "produto":
{ "codigo": 123, "nome": "Nome do Produto", "valor": 100 },
"condicaoPagamento":
{ "valorEntrada": 10, "qtdeParcelas": 0 }
}
```

### Output
```
{
    "error: ": "Impossível calcular. O numero de parcelas deve ser maior a 1."
}
```