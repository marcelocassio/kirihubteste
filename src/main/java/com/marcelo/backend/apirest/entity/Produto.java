package com.marcelo.backend.apirest.entity;

/**
 * Modelo principal do payload
 * @author marce
 *
 */

public class Produto {

	private DetalheProduto produto;
	private CondicaoPagamento condicaoPagamento;

	public DetalheProduto getProduto() {
		return produto;
	}

	public void setProduto(DetalheProduto produto) {
		this.produto = produto;
	}

	public CondicaoPagamento getCondicaoPagamento() {
		return condicaoPagamento;
	}

	public void setCondicaoPagamento(CondicaoPagamento condicaoPagamento) {
		this.condicaoPagamento = condicaoPagamento;
	}

}
