package com.marcelo.backend.apirest.entity;

/**
 * Modelo de detalhe do produto
 * 
 * @author marce
 *
 */
public class DetalheProduto {

	private Long codigo;
	private String nome;
	private double valor;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
