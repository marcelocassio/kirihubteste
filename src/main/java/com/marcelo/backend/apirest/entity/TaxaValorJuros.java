package com.marcelo.backend.apirest.entity;


/**
 * Objeto para popular com os dados de calculo de taxa de juros e valor dos juros
 * @author marce
 *
 */
public class TaxaValorJuros {
	
	public TaxaValorJuros(double taxaMensal, double valorTotalJuros) {
		super();
		this.taxaMensal = taxaMensal;
		this.valorTotalJuros = valorTotalJuros;
	}

	private double taxaMensal;
	private double valorTotalJuros;

	public double getTaxaMensal() {
		return taxaMensal;
	}

	public void setTaxaMensal(double taxaMensal) {
		this.taxaMensal = taxaMensal;
	}

	public double getValorTotalJuros() {
		return valorTotalJuros;
	}

	public void setValorTotalJuros(double valorTotalJuros) {
		this.valorTotalJuros = valorTotalJuros;
	}

}
