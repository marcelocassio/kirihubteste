package com.marcelo.backend.apirest.entity;

/**
 * Objeto para popular a resposta do service de SELIC
 * @author marce
 *
 */
public class TaxaSelic {
	
	private String data;
	private String valor;
	
	
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	

}
