package com.marcelo.backend.apirest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.marcelo.backend.apirest.dto.ParcelaDto;
import com.marcelo.backend.apirest.entity.Produto;
import com.marcelo.backend.apirest.exception.ValidarDados;
import com.marcelo.backend.apirest.models.services.IGerarPagamentoService;
import com.marcelo.backend.config.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api")
@Api(tags = { SwaggerConfig.PARCELA_TAG })
public class CompraProdutoController {

	@Autowired
	IGerarPagamentoService service;

	@ApiOperation(value = "Enpoint para gera os detalhes do pagamento do produto", response = ParcelaDto.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Parcelas geradas com exito."),
			@ApiResponse(code = 400, message = "Erros de validacao de dados"),
			@ApiResponse(code = 500, message = "Houve um erro ao calcular as parcelas.") })
	@PostMapping("/gerarPagamento")
	public ResponseEntity<Object> gerarCondicaoPagamento(@RequestBody Produto produto) {

		Map<String, Object> response = new HashMap<>();
		List<ParcelaDto> list = new ArrayList<>();

		try {
			ValidarDados.validar(produto);
		} catch (Exception e) {
			response.put("error: ", e.getMessage());
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			list = service.getDetalhePagamento(produto);
		} catch (Exception e) {
			response.put("error: ", "Houve um erro ao calcular as parcelas.");
			response.put("detail: ", e.getMessage());
			return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Object>(list, HttpStatus.OK);
	}

}
