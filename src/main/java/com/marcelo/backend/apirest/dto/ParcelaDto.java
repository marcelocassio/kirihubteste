package com.marcelo.backend.apirest.dto;

/**
 * Data transfer Object de detalhe da parcela - Output
 * @author marce
 *
 */
public class ParcelaDto {

	private int numeroParcela;
	private Double valor;
	private Double taxaJurosAoMes;

	public int getNumeroParcela() {
		return numeroParcela;
	}

	public void setNumeroParcela(int numeroParcela) {
		this.numeroParcela = numeroParcela;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getTaxaJurosAoMes() {
		return taxaJurosAoMes;
	}

	public void setTaxaJurosAoMes(Double taxaJurosAoMes) {
		this.taxaJurosAoMes = taxaJurosAoMes;
	}

}
