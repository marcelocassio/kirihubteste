package com.marcelo.backend.apirest.utils;


/**
 * Variaveis constantes do sistema
 * @author marce
 *
 */
public final class Constantes {
	
	
	public static final String VALOR_ZERO = "O valor do produto é invalido. Por favor verifique.";
	public static final String VALOR_MAIOR = "O valor da entrada nao pode ser maior que o valor do produto.";
	public static final String VALOR_IGUAL = "O valor da entrada é igual ao valor do produto. O pagamento é a vista.";
	public static final String VALOR_MENOR = "O valor da entrada é invalido. Por favor verifique.";
	public static final String PARCELAS_MAIOR_ZERO = "Impossível calcular. O numero de parcelas deve ser maior a 1.";

}
