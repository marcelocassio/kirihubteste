package com.marcelo.backend.apirest.exception;

import com.marcelo.backend.apirest.entity.Produto;
import com.marcelo.backend.apirest.utils.Constantes;

/**
 * Clase para validar a entrada de dados
 * 
 * @author marce
 *
 */
public class ValidarDados {

	public static void validar(Produto produto) throws DadosInvalidosException {

		if (produto.getProduto().getValor() <= 0) {
			throw new DadosInvalidosException(Constantes.VALOR_ZERO);
		}
		if (produto.getCondicaoPagamento().getValorEntrada() > produto.getProduto().getValor()) {
			throw new DadosInvalidosException(Constantes.VALOR_MAIOR);
		}

		if (produto.getCondicaoPagamento().getValorEntrada() == produto.getProduto().getValor()) {
			throw new DadosInvalidosException(Constantes.VALOR_IGUAL);
		}
		
		if (produto.getCondicaoPagamento().getValorEntrada() < 0 ) {
			throw new DadosInvalidosException(Constantes.VALOR_MENOR);
		}

		if (produto.getCondicaoPagamento().getQtdeParcelas() < 1) {
			throw new DadosInvalidosException(Constantes.PARCELAS_MAIOR_ZERO);
		}
	}
}
