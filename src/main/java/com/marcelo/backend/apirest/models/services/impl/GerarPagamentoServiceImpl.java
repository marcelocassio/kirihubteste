package com.marcelo.backend.apirest.models.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.marcelo.backend.apirest.dto.ParcelaDto;
import com.marcelo.backend.apirest.entity.Produto;
import com.marcelo.backend.apirest.entity.TaxaSelic;
import com.marcelo.backend.apirest.entity.TaxaValorJuros;
import com.marcelo.backend.apirest.exception.ValidarDados;
import com.marcelo.backend.apirest.exception.DadosInvalidosException;
import com.marcelo.backend.apirest.models.services.IGerarPagamentoService;
import com.marcelo.backend.apirest.utils.Uteis;

@Service
public class GerarPagamentoServiceImpl implements IGerarPagamentoService {

	@Autowired
	private RestTemplate restTemplate;

	private double taxaSelicMensal;

	@Value("${url_selic}")
	private String urlSelic;

	/**
	 * Servico que gera os dados de pagamento
	 * @throws DadosInvalidosException 
	 */
	@Override
	public List<ParcelaDto> getDetalhePagamento(Produto produto) throws DadosInvalidosException {

		List<ParcelaDto> lista = new ArrayList<ParcelaDto>();
		double valorAParcelar = produto.getProduto().getValor() - produto.getCondicaoPagamento().getValorEntrada();
		int qtParcelas = produto.getCondicaoPagamento().getQtdeParcelas();

		TaxaValorJuros taxaJuros = calcularValorJuros(valorAParcelar, qtParcelas);
		double valorParcela = calcularValorParcela(valorAParcelar, taxaJuros.getValorTotalJuros(), qtParcelas);

		for (int i = 1; i <= qtParcelas; i++) {
			ParcelaDto parcela = new ParcelaDto();
			parcela.setNumeroParcela(i);
			parcela.setValor(valorParcela);
			parcela.setTaxaJurosAoMes(taxaJuros.getTaxaMensal());
			lista.add(parcela);
		}

		return lista;
	}

	/**
	 * Método que devolve o valor total de juros e taxa mensal da operacao. Somente
	 * devolve juros se a quantidade de parcelas é superior a 6 meses. Se recupera a
	 * taxa de juros SELIC do ultimo mes. Caso as parcelas sao de 6 meses o
	 * inferior, o metodo devolve o valor 0 porque nao se cobra juros de
	 * financiamento.
	 * 
	 * @param valor
	 * @param ctParcelas
	 * @return valor total de juros que se deve pagar e a taxa mensal - Devolve em
	 *         um objeto.
	 */
	private TaxaValorJuros calcularValorJuros(double valor, int qtParcelas) {
		TaxaValorJuros txj = new TaxaValorJuros(0, 0);
		if (qtParcelas > 6) {
			double taxaMensal = getTaxaSelic();
			txj.setTaxaMensal(taxaMensal);
			txj.setValorTotalJuros((valor * taxaMensal * qtParcelas) / 100);
		}
		return txj;
	}

	/**
	 * Método que calcular o valor por parcela
	 * 
	 * @param capital
	 * @param valorJuros
	 * @param qtParcelas
	 * @return valor a pagar por parcela
	 */
	public double calcularValorParcela(double capital, double valorJuros, int qtParcelas) {
		Uteis.darFormato((capital + valorJuros) / qtParcelas);
		return Uteis.darFormato((capital + valorJuros) / qtParcelas);
	}

	/**
	 * Metodo que recupera a taxa selic online e calcula o ultimo mes - somente dias
	 * - Se há algum erro nessa requisisao, retorna o valor padrao 1.15. uteis
	 * 
	 * @return valor mensal da taxa de juros
	 */
	private double getTaxaSelic() {
		String url = urlSelic;
		try {
			List<TaxaSelic> listaTaxasTotal = Arrays.asList(restTemplate.getForObject(url, TaxaSelic[].class));
			List<TaxaSelic> taxaMensal = listaTaxasTotal.subList((listaTaxasTotal.size() - 22), listaTaxasTotal.size());
			taxaMensal.forEach(t -> {
				taxaSelicMensal += Double.valueOf(t.getValor());
			});
			return Uteis.darFormato(taxaSelicMensal);
		} catch (Exception e) {
			System.err.println("Houve um erro ao obtener a taxa SELIC: " + e.getMessage());
			return 1.15;
		}

	}

}
