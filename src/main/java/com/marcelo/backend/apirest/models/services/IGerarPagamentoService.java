package com.marcelo.backend.apirest.models.services;

import java.util.List;

import com.marcelo.backend.apirest.dto.ParcelaDto;
import com.marcelo.backend.apirest.entity.Produto;
import com.marcelo.backend.apirest.exception.DadosInvalidosException;

public interface IGerarPagamentoService {

	public List<ParcelaDto> getDetalhePagamento(Produto produto) throws DadosInvalidosException;

}
