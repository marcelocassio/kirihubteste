package com.marcelo.backend.apirest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.marcelo.backend.apirest.models.services.impl.GerarPagamentoServiceImpl;

@SpringBootTest
@RunWith(BlockJUnit4ClassRunner.class)
class GerarPagamentoServiceImplTest {

	GerarPagamentoServiceImpl gpsImpl;

	@BeforeEach
	void setUp() throws Exception {
		gpsImpl = new GerarPagamentoServiceImpl();

	}

	@Test
	void testCalcularValorParcela() {

		double resultado = gpsImpl.calcularValorParcela(100, 10, 10);
		double esperado = 11;
		Assertions.assertEquals(esperado, resultado);
	}

}
